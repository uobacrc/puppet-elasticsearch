# == Class: elasticsearch
#
# Full description of class elasticsearch here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { elasticsearch:
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2016 Your name here, unless otherwise noted.
#
class elasticsearch (
  $cluster_name = 'my-application' 
) {

  yumrepo { 'elasticsearch':
    descr     => 'Elasticsearch upstream repo',
    baseurl  => 'http://packages.elastic.co/elasticsearch/2.x/centos',
    gpgcheck => 1,
    gpgkey   => 'http://packages.elastic.co/GPG-KEY-elasticsearch',
    enabled  => 1,
  }

  package { 'elasticsearch':
    ensure => installed,
    require => Yumrepo['elasticsearch'],
  }


  service { 'elasticsearch':
    ensure => running,
    require => Package['elasticsearch'],
  }

  file { '/etc/elasticsearch/elasticsearch.yml':
    ensure => file,
    owner => 'root',
    group => 'elasticsearch',
    mode => 0750,
    content => template('elasticsearch/elasticsearch.yml.erb'),
  }
 
}
